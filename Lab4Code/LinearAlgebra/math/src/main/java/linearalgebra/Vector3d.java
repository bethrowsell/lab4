//Beth Rowsell
//2135123

package linearalgebra;
import java.lang.Math;

public class Vector3d{
    double x;
    double y;
    double z;

    public Vector3d(double a, double b, double c){
        x = a;
        y = b;
        z = c;
    }

    public static void main(String[] args){
        Vector3d v1 = new Vector3d(2, 4, 6);
        Vector3d v2 = new Vector3d(1, 3, 5);
        System.out.println(v1.magnitude());
        System.out.println(v1.dotProduct(v2));
        System.out.println(v1.add(v2));
    }

    public double getX(){
        return x;
    }

    public double getY(){
        return y;
    }

    public double getZ(){
        return z;
    }

    public double magnitude(){
        return Math.sqrt(x*x + y*y + z*z);
    }

    public double dotProduct(Vector3d v){
        return (x*v.getX()) + (y*v.getY()) + (z*v.getZ());
    }

    public double add(Vector3d v){
        return (x + v.getX()) + (y + v.getY()) + (z + v.getZ());
    }
}