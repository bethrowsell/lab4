//Beth Rowsell
//2135123

package linearalgebra;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class Vector3dTests {

    @Test
    public void testingGetMethods(){
        Vector3d v1 = new Vector3d(2, 4, 6);
        assertEquals("Testing method get X", 2.0, v1.getX(), 0.000001);
        assertEquals("Testing method get Y", 4.0, v1.getY(), 0.000001);
        assertEquals("Testing method get Z", 6.0, v1.getZ(), 0.000001);
    }
    

    @Test
    public void testingMagnitude(){
        Vector3d v2 = new Vector3d(1, 3, 5);
        assertEquals("Testing method magnitude", 5.916079783099616, v2.magnitude(), 0.000001);
    }

    @Test
    public void testingDotProduct(){
        Vector3d v3 = new Vector3d(2, 4, 6);
        Vector3d v4 = new Vector3d(1, 3, 5);
        assertEquals("Testing method dot product", 44.0, v3.dotProduct(v4), 0.000001);
    }

    @Test
    public void testingAdd(){
        Vector3d v5 = new Vector3d(2, 4, 6);
        Vector3d v6 = new Vector3d(1, 3, 5);
        assertEquals("Testing method dot product", 21.0, v5.add(v6), 0.000001);
    }    
}
